﻿#include <iostream>
#include <time.h>


const int N = 4;
int array[N][N];
int i = 0, j = 0; 

int main()
{

    for (i = 0; i < N; i++)
    {
        for (j = 0; j < N; j++)
        {
            array[i][j] = i + j;
        }
    }

    for (i = 0; i < N; i++)
    {
        for (j = 0; j < N; j++)
        {
            std::cout << array[i][j];
        }
        std::cout << "\n";
    }
     
    setlocale(LC_ALL, "rus");

    int n, m, N, sum = 0;

    std::cout << "Введите значение N: ";

    std::cin >> N;

    time_t t;

    time(&t);

    int k = (24) % N;

    std::cout << "Введите размерность массива: ";

    std::cin >> n >> m;

    int** a = new int* [n];

    for (int i = 0; i < n; i++)
        a[i] = new int[m];

    std::cout << "Введите элементы массива:\n";

    for (int i = 0; i < n; i++)
        for (int j = 0; j < m; j++) 
        {
            std::cin >> a[i][j];
            if (i == k) sum += a[i][j];
        }
    std::cout << "Сумма элементов строки " << k << " = " << sum;

    for (int i = 0; i < n; i++)
        delete[]a[i];
        delete[] a;

        return 0;
}